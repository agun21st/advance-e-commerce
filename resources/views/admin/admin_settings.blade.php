@extends('layouts.Admin.admin_layout')
@section('content')
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Settings</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Admin Settings</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-user-cog"></i> Update Profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ url('/admin/update-current-password') }}" method="post" id="admin-profile">
                @csrf
        
                {{-- Successful Message Alert with close and fade effect from controller --}}
                @if (session('error_password_update'))
                {{-- <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Success!</strong> You have been signed in successfully!
                </div> --}}
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('error_password_update')}}
                </div>
                @endif
                @if (session('success_message'))
                {{-- <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Success!</strong> You have been signed in successfully!
                </div> --}}
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('success_message')}}
                </div>
                @endif
        
                @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                @endif

                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Admin Name</label>
                    <input type="text" placeholder="Enter Admin/Sub Admin Name" class="form-control" name="name" id="name" value="{{$current_user->name}}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="email">Admin Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{$current_user->email}}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="type">Admin Type</label>
                    <input type="text" class="form-control" id="type" value="{{$current_user->type}}" name="type" readonly>
                  </div>
                  <div class="form-group">
                    <label for="current_password">Current Password</label>
                    <input type="password" class="form-control" id="current_password" name="current_password" placeholder="Enter Current Password" required>
                    <span id="verifyCPassword"></span>
                  </div>
                  <div class="form-group">
                    <label for="new_password">New Password</label>
                    <input type="password" class="form-control" id="new_password" name="password" placeholder="Enter New Password" required>
                  </div>
                  <div class="form-group">
                    <label for="confirm_password">Confirm Password</label>
                    <input type="password" class="form-control" id="confirm_password" name="password_confirmation" placeholder="Confirm Password" required>
                  </div>
                </div>
                <!-- /.card-body -->
        
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>


  </div>
  <!-- /.content-wrapper -->
@endsection