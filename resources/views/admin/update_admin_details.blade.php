@extends('layouts.Admin.admin_layout')
@section('content')
      <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Admin Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title"><i class="fas fa-user-cog"></i> Update Admin Details</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="{{ url('/admin/update-admin-details') }}" method="post" id="admin-profile" enctype="multipart/form-data">
                @csrf
        
                {{-- Successful Message Alert with close and fade effect from controller --}}
                @if (session('error_password_update'))
                {{-- <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Success!</strong> You have been signed in successfully!
                </div> --}}
                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('error_password_update')}}
                </div>
                @endif
                @if (session('success_message'))
                {{-- <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Success!</strong> You have been signed in successfully!
                </div> --}}
                <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    {{session('success_message')}}
                </div>
                @endif
        
                @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
                @endif

                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Admin Name</label>
                    <input type="text" placeholder="Enter Admin/Sub Admin Name" class="form-control" name="name" id="name" value="{{$current_user->name}}" required>
                  </div>
                  <div class="form-group">
                    <label for="email">Admin Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{$current_user->email}}" readonly>
                  </div>
                  <div class="form-group">
                    <label for="type">Admin Type</label>
                    <input type="text" class="form-control" id="type" value="{{$current_user->type}}" name="type" readonly>
                  </div>
                  <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input type="text" class="form-control" id="mobile" name="mobile" value="{{$current_user->mobile}}" placeholder="Enter mobile number">
                  </div>
                  <div class="form-group">
                    <label for="image">Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                        <label class="custom-file-label" for="image">Choose image</label>
                      </div>
                  </div>
                </div>
                </div>
                <!-- /.card-body -->
        
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>


  </div>
  <!-- /.content-wrapper -->
@endsection