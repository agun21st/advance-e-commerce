@extends('layouts.Admin.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Products</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Products</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div>
      @if (session('success_message'))
        {{-- <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success!</strong> You have been signed in successfully!
        </div> --}}
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{session('success_message')}}
        </div>
        @endif
        {{-- Successful Message Alert with close and fade effect from controller --}}
        @if (session('error_message'))
        {{-- <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success!</strong> You have been signed in successfully!
        </div> --}}
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{session('error_message')}}
        </div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">All Products</h3>
                  <a href="{{route('products.create')}}" class="btn btn-block btn-primary" style="max-width: 150px; float: right;">Add Product</a>                
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  {{-- <table id="example1" class="table table-bordered table-striped"> --}}
                    <table id="example1" class="table table-bordered table-hover">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Action</th>
                          <th>Section</th>
                          <th>Brand</th>
                          <th>Category</th>
                          <th>Name</th>
                          <th>Code</th>
                          <th>Color</th>
                          <th>Price</th>
                          <th>Main Image</th>
                          <th>Video</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($products as $product)    
                        <tr>
                          <td>{{ $product->id}}</td>
                          <td class="d-flex justify-content-around flex-wrap">
                            @if ($product->status==1)
                                <a type="button" class="updateProductStatus" id="product-{{$product->id}}"
                                  product_id="{{$product->id}}" href="javascript:void(0)" title="Inactive"><i class="fas fa-toggle-on" status="Active"></i></a>
                              @else
                                <a type="button" class="updateProductStatus" id="product-{{$product->id}}"
                                  product_id="{{$product->id}}" href="javascript:void(0)" title="Active"><i class="fas fa-toggle-off" status="Inactive" style="color:red"></i></a>
                              @endif
                            <a href="{{url('/admin/attributes/'.$product->id)}}" title="Add Attributes"><i class="fas fa-plus"></i></a>
                            <a href="{{url('/admin/productImages/'.$product->id)}}" title="Add Images"><i class="far fa-images"></i></a>
                            <a href="{{url('/admin/products/'.$product->id.'/edit')}}" title="Edit Product"><i class="nav-icon fas fa-edit"></i></a>
                            <a class="confirmDelete" href="javascript:void(0)" recordName="product" record="deleteProduct"  recordId="{{$product->id}}" title="Delete Product" style="color:red"><i class="far fa-trash-alt"></i></a>
                            {{-- <form class="confirmDelete" name="product" action="{{url('admin/products/'.$product->id)}}" method="POST"> --}}
                              {{-- @csrf  --}}
                              {{-- cross-site request forgery
                                  Laravel makes it easy to protect 
                                  your application from cross-site request forgery (CSRF) 
                                  attacks. Cross-site request forgeries are a type of 
                                  malicious exploit whereby unauthorized commands are 
                                  performed on behalf of an authenticated user.  --}}
                              {{-- @method('delete')
                              <button type="submit" class="btn btn-danger btn-sm" title="Delete"><i class="far fa-trash-alt"></i></button>
                            </form> --}}
                          </td>
                          <td>{{ $product->section->name}}</td>
                          <td>{{ $product->brand->name}}</td>
                          <td>{{ $product->category->name}}</td>
                          <td>{{ $product->name}}</td>
                          <td>{{ $product->code}}</td>
                          <td>{{ $product->color}}</td>
                          <td>{{ $product->price}}</td>
                          <td class="text-center">
                            @if (!empty($product->main_image) && file_exists('images/product_images/small/'.$product->main_image))
                              <img src="{{ asset('images/product_images/small/'.$product->main_image)}}" width="50px" alt="product_image" />
                            @else
                            <img src="https://via.placeholder.com/50x65"/>
                            @endif
                          </td>
                          <td class="text-center">
                            @if (!empty($product->video) && file_exists('videos/product_videos/'.$product->video))
                              {{-- <video width="120" height="80" controls>
                                  <source src="{{asset('videos/product_videos/'.$product->video)}}" type="video/mp4">
                                  Your browser does not support HTML video.
                              </video> --}}
                              <a class="ml-2 btn btn-info btn-sm" href="{{asset('videos/product_videos/'.$product->video)}}" title="Download Video" download><i class="fas fa-download"></i></a>
                            @else
                            @endif
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    <tfoot>
                        <tr>
                          <th>#</th>
                          <th>Action</th>
                          <th>Section</th>
                          <th>Brand</th>
                          <th>Category</th>
                          <th>Name</th>
                          <th>Code</th>
                          <th>Color</th>
                          <th>Price</th>
                          <th>Main Image</th>
                          <th>Video</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->


</div>
  <!-- /.content-wrapper -->
@endsection