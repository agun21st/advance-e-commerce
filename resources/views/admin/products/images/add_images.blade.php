@extends('layouts.Admin.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product Images</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Add Product Images</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Add Product Images</h3>
                    <a href="{{route('products.index')}}" class="btn btn-block btn-primary" style="max-width: 150px; float: right;">All Products</a>
                    {{-- <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div> --}}
                </div>
                <div>
                    @if (session('success_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('success_message')}}
                        </div>
                        @endif
                        {{-- Successful Message Alert with close and fade effect from controller --}}
                        @if (session('error_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('error_message')}}
                        </div>
                        @endif
                
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <!-- /.card-header -->
                <form action="{{ route('productImages.store') }}" method="post" enctype="multipart/form-data" id="productImages_form" >
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Product Name:</label>&nbsp;&nbsp;{{ $getProductImages->name }}
                                </div>
                                <div class="form-group">
                                    <label for="code">Product Code:</label>&nbsp;&nbsp;{{ $getProductImages->code }}
                                </div>
                                <div class="form-group">
                                    <label for="color">Product Color:</label>&nbsp;&nbsp;{{ $getProductImages->color }}
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                @if (!empty($getProductImages->main_image) && file_exists('images/product_images/small/'.$getProductImages->main_image))
                                    <img src="{{ asset('images/product_images/small/'.$getProductImages->main_image)}}" width="80px" alt="product_image" />
                                @else
                                    <img src="https://via.placeholder.com/50x65" width="80px" alt="product_image" />
                                @endif
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="image_field_wrapper">
                                    <div class="form-group mt-2">
                                        <input type="hidden" name="product_id" value="{{$getProductImages->id}}"> 
                                        <label for="productImage">Add Product Image</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="productImage" name="productImage[]" accept="image/*" multiple required>
                                                <label class="custom-file-label" for="productImage">Choose image (720 x 1080)</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Add Image</button>
                    </div>
                </form>
            </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Added Attribute List</h3>
                </div>
                    <div class="card-body">
                        {{-- <table id="example1" class="table table-bordered table-striped"> --}}
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($getProductImages->images as $productImage)    
                                    <tr>
                                        <td>{{ $productImage->id }}</td>
                                        <input type="hidden" name="productImageId[]" id="productImageId" value="{{ $productImage->id}}"/>
                                        <td class="text-center">
                                            @if (!empty($productImage->image) && file_exists('images/product_images/small/'.$productImage->image))
                                            <img src="{{ asset('images/product_images/small/'.$productImage->image)}}" width="50px" alt="image" />
                                            @else
                                            <img src="https://via.placeholder.com/50x65"/>
                                            @endif
                                        </td>
                                        <td class="d-flex justify-content-around">
                                            @if ($productImage->status==1)
                                                <a type="button" class="updateProductImageStatus" id="productImage-{{$productImage->id}}"
                                                productImage_id="{{$productImage->id}}" href="javascript:void(0)" title="Inactive"><i class="fas fa-toggle-on" status="Active"></i></a>
                                            @else
                                                <a type="button" class="updateProductImageStatus" id="productImage-{{$productImage->id}}"
                                                productImage_id="{{$productImage->id}}" href="javascript:void(0)" title="Active"><i class="fas fa-toggle-off" status="Inactive" style="color:red"></i></a>
                                            @endif
                                            <a class="confirmDelete" href="javascript:void(0)" recordName="Product_Image" record="deleteProductImage"  recordId="{{$productImage->id}}" title="Delete Image"><i class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Action</th>
                                    <th>Image</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
            </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>


</div>
  <!-- /.content-wrapper -->
@endsection