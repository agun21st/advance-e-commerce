@extends('layouts.Admin.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product Attributes</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Add Product Attributes</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Add Product Attributes</h3>
                    <a href="{{route('products.index')}}" class="btn btn-block btn-primary" style="max-width: 150px; float: right;">All Products</a>
                    {{-- <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div> --}}
                </div>
                <div>
                    @if (session('success_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('success_message')}}
                        </div>
                        @endif
                        {{-- Successful Message Alert with close and fade effect from controller --}}
                        @if (session('error_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('error_message')}}
                        </div>
                        @endif
                
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <!-- /.card-header -->
                <form action="{{ route('attributes.store') }}" method="post" enctype="multipart/form-data" id="attributes_form" >
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Product Name:</label>&nbsp;&nbsp;{{ $getProduct->name }}
                                </div>
                                <div class="form-group">
                                    <label for="code">Product Code:</label>&nbsp;&nbsp;{{ $getProduct->code }}
                                </div>
                                <div class="form-group">
                                    <label for="color">Product Color:</label>&nbsp;&nbsp;{{ $getProduct->color }}
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                @if (!empty($getProduct->main_image) && file_exists('images/product_images/small/'.$getProduct->main_image))
                                    <img src="{{ asset('images/product_images/small/'.$getProduct->main_image)}}" width="80px" alt="product_image" />
                                @else
                                    <img src="https://via.placeholder.com/50x65" width="80px" alt="product_image" />
                                @endif
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="field_wrapper">
                                    <div class="mt-2">
                                        <input type="hidden" name="product_id" value="{{$getProduct->id}}"> 
                                        <input type="text" name="size[]" id="size" placeholder="SIZE" value="" style="width: 120px;" required/>
                                        <input type="number" name="price[]" id="price" placeholder="PRICE" value="" style="width: 120px;" required/>
                                        <input type="number" name="stock[]" id="stock" placeholder="STOCK" value="" style="width: 120px;" required/>
                                        <input type="text" name="sku[]" id="sku" placeholder="SKU" value="" style="width: 120px;" required/>
                                        <a href="javascript:void(0);" class="add_button" title="Add field">&nbsp;&nbsp;Add</a>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Add Attributes</button>
                    </div>
                </form>
            </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Added Attribute List</h3>
                </div>
                <form action="{{ url('/admin/attributes/'.$getProduct->id) }}" method="post" enctype="multipart/form-data" id="attributes_update_form" >
                    @csrf
                    @method('patch')
                    <div class="card-body">
                        {{-- <table id="example1" class="table table-bordered table-striped"> --}}
                        <table id="example1" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Action</th>
                                    <th>Size</th>
                                    <th>SKU</th>
                                    <th>Price</th>
                                    <th>Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($getProduct->attributes as $attribute)    
                                    <tr>
                                        <td>{{ $attribute->id }}</td>
                                        <input type="hidden" name="attributeId[]" id="attributeId" value="{{ $attribute->id}}"/>
                                        <td class="d-flex justify-content-around">
                                            @if ($attribute->status==1)
                                                <a type="button" class="updateAttributeStatus" id="attribute-{{$attribute->id}}"
                                                attribute_id="{{$attribute->id}}" href="javascript:void(0)" title="Inactive"><i class="fas fa-toggle-on" status="Active"></i></a>
                                            @else
                                                <a type="button" class="updateAttributeStatus" id="attribute-{{$attribute->id}}"
                                                attribute_id="{{$attribute->id}}" href="javascript:void(0)" title="Active"><i class="fas fa-toggle-off" status="Inactive" style="color:red"></i></a>
                                            @endif
                                            <a class="confirmDelete" href="javascript:void(0)" recordName="attribute" record="deleteAttribute"  recordId="{{$attribute->id}}" title="Delete Attribute"><i class="far fa-trash-alt"></i></a>
                                        </td>
                                        <td>{{ $attribute->size}}</td>
                                        <td>{{ $attribute->sku}}</td>
                                        <td>
                                            <input type="number" name="price[]" id="price" placeholder="Price" value="{{ $attribute->price}}" style="width: 80px;" required/>
                                        </td>
                                        <td>
                                            <input type="number" name="stock[]" id="stock" placeholder="stock" value="{{ $attribute->stock}}" style="width: 80px;" required/>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Action</th>
                                    <th>Size</th>
                                    <th>SKU</th>
                                    <th>Price</th>
                                    <th>Stock</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-info">Update Attributes</button>
                    </div>
                </form>
            </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>


</div>
  <!-- /.content-wrapper -->
@endsection