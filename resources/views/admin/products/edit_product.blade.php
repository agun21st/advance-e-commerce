@extends('layouts.Admin.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Products</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Edit Product</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Edit Product</h3>
                    <a href="{{route('products.index')}}" class="btn btn-block btn-primary" style="max-width: 150px; float: right;">All Products</a>
                    {{-- <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div> --}}
                </div>
                <div>
                    @if (session('success_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('success_message')}}
                        </div>
                        @endif
                        {{-- Successful Message Alert with close and fade effect from controller --}}
                        @if (session('error_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('error_message')}}
                        </div>
                        @endif
                
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <!-- /.card-header -->
                <form action="{{ url('/admin/products/'.$product->id) }}" method="post" enctype="multipart/form-data" id="product_form">
                    @csrf
                    @method('patch')

                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="section_id_for_product">Sections*</label>
                                    <select class="form-control select2" style="width: 100%;" name="section_id_for_product" id="section_id_for_product" required>
                                        @foreach ($sections as $section)
                                            <option value="{{$section->id}}" {{$product->section_id==$section->id?'selected':''}}>{{$section->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="brand_id_for_product">Brands*</label>
                                    <select class="form-control select2" style="width: 100%;" name="brand_id_for_product" id="brand_id_for_product" required>
                                        @foreach ($brands as $brand)
                                            <option value="{{$brand->id}}" {{$product->brand_id==$brand->id?'selected':''}}>{{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name">Product Name*</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $product->name }}" placeholder="Enter Product Name" required>
                                </div>
                                <div class="form-group">
                                    <label for="color">Product Color*</label>
                                    <input type="text" class="form-control" id="color" name="color" value="{{ $product->color }}" placeholder="Enter Product Color" required>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Product Discount(%)</label>
                                    <input type="text" class="form-control" id="discount" name="discount" value="{{ $product->discount }}" placeholder="Enter Product Discount">
                                </div>
                                <div class="form-group">
                                    <label for="image">Product Image</label>
                                    <div class="input-group">
                                      <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="main_image" accept="image/*">
                                        <label class="custom-file-label" for="image">Choose image (720 x 1080)</label>
                                      </div>
                                    </div>
                                </div>
                                @if (!empty($product->main_image) && file_exists('images/product_images/small/'.$product->main_image))
                                    <div class="form-group">
                                        <img  id="main_image" src="{{asset('/images/product_images/small/'.$product->main_image)}}" width="90px" />
                                        <a class="confirmDelete ml-5 btn btn-danger btn-sm" href="javascript:void(0)" recordName="Product_Main_Image" record="deleteProductMainImage"  recordId="{{$product->id}}" title="Delete Image"><i class="far fa-trash-alt"></i></a>
                                        <a class="ml-2 btn btn-info btn-sm" href="{{asset('/images/product_images/small/'.$product->main_image)}}" title="Download Image" download><i class="fas fa-download"></i></a>
                                    </div>
                                @else
                                @endif
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="category_id_for_product">Categories*</label>
                                    <select class="form-control select2" style="width: 100%;" name="category_id_for_product" id="category_id_for_product" required>
                                        @if (!empty($categories))    
                                            @foreach ($categories as $category)
                                                <option value="{{$category->id}}" {{$category->id==$product->category_id?"selected":""}}>{{$category->name}}</option>
                                                @if (!empty($category->sub_categories))    
                                                    @foreach ($category->sub_categories as $subCategory)
                                                        <option value="{{$subCategory->id}}" {{$subCategory->id==$product->category_id?"selected":""}}>&nbsp;&nbsp;--&nbsp;{{$subCategory->name}}</option>
                                                    @endforeach
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="code">Product Code*</label>
                                    <input type="text" class="form-control" id="code" name="code" value="{{ $product->code }}" placeholder="Enter Product Code" required>
                                </div>
                                <div class="form-group">
                                    <label for="price">Product Price*</label>
                                    <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}" placeholder="Enter Product Price" required>
                                </div>
                                <div class="form-group">
                                    <label for="weight">Product Weight</label>
                                    <input type="text" class="form-control" id="weight" name="weight" value="{{ $product->weight }}" placeholder="Enter Product Weight" required>
                                </div>
                                <div class="form-group">
                                    <label for="video">Product Video</label>
                                    <div class="input-group">
                                      <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="video" name="video" accept="video/mp4">
                                        <label class="custom-file-label" for="image">Choose Video (.mp4 max/20mb)</label>
                                      </div>
                                    </div>
                                </div>
                                @if (!empty($product->video) && file_exists('videos/product_videos/'.$product->video))
                                    <div class="form-group">
                                        <video width="160" height="120" controls>
                                            <source src="{{asset('videos/product_videos/'.$product->video)}}" type="video/mp4">
                                            Your browser does not support HTML video.
                                        </video>
                                        <a class="confirmDelete ml-5 btn btn-danger btn-sm" href="javascript:void(0)" recordName="Product_Video" record="deleteProductVideo"  recordId="{{$product->id}}" title="Delete Video"><i class="far fa-trash-alt"></i></a>
                                        <a class="ml-2 btn btn-info btn-sm" href="{{asset('videos/product_videos/'.$product->video)}}" title="Download Video" download><i class="fas fa-download"></i></a>
                                    </div>
                                @else
                                @endif
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                        <!-- row -->
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="description">Product Description</label>
                                    <textarea class="form-control" rows="5" placeholder="Enter Description" id="description" name="description">{{ $product->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="wash_care">Product Wash Care*</label>
                                    <input type="text" class="form-control" id="wash_care" name="wash_care" value="{{ $product->wash_care }}" placeholder="Enter Product Wash Care">
                                </div>
                                <div class="form-group">
                                    <label for="fabric">Fabrics</label>
                                    <select class="form-control select2" style="width: 100%;" name="fabric" id="fabric">
                                        <option selected="selected">Select Fabric</option>
                                        @foreach ($fabrics as $fabric)
                                            <option value="{{$fabric}}" @if($product->fabric==$fabric) selected @endif>{{$fabric}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="sleeve">Sleeves</label>
                                    <select class="form-control select2" style="width: 100%;" name="sleeve" id="sleeve">
                                        <option selected="selected">Select Sleeve</option>
                                        @foreach ($sleeves as $sleeve)
                                            <option value="{{$sleeve}}" @if($product->sleeve==$sleeve) selected @endif>{{$sleeve}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="pattern">Patterns</label>
                                    <select class="form-control select2" style="width: 100%;" name="pattern" id="pattern">
                                        <option selected="selected">Select Pattern</option>
                                        @foreach ($patterns as $pattern)
                                            <option value="{{$pattern}}" @if($product->pattern==$pattern) selected @endif>{{$pattern}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="fit">Fits</label>
                                    <select class="form-control select2" style="width: 100%;" name="fit" id="fit">
                                        <option selected="selected">Select Fit</option>
                                        @foreach ($fits as $fit)
                                            <option value="{{$fit}}" @if($product->fit==$fit) selected @endif>{{$fit}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="occasion">Occasions</label>
                                    <select class="form-control select2" style="width: 100%;" name="occasion" id="occasion">
                                        <option selected="selected">Select Occasion</option>
                                        @foreach ($occasions as $occasion)
                                            <option value="{{$occasion}}" @if($product->occasion==$occasion) selected @endif>{{$occasion}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="meta_title">Meta Title</label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Meta Title" id="meta_title" name="meta_title">{{ $product->meta_title }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_description">Meta Description</label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Meta Description" id="meta_description" name="meta_description">{{ $product->meta_description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_keywords">Meta Keywords</label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Meta Keywords" id="meta_keywords" name="meta_keywords">{{ $product->meta_keywords }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_keywords">Featured Item</label>
                                    <input type="checkbox" name="is_featured" id="is_featured" {{$product->is_featured=='Yes'?'checked':''}}>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success btn-block">Update</button>
                    </div>
                </form>
            </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>


</div>
  <!-- /.content-wrapper -->
@endsection