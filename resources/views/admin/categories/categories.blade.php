@extends('layouts.Admin.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Catalogues</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Categories</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div>
      @if (session('success_message'))
        {{-- <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success!</strong> You have been signed in successfully!
        </div> --}}
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{session('success_message')}}
        </div>
        @endif
        {{-- Successful Message Alert with close and fade effect from controller --}}
        @if (session('error_message'))
        {{-- <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Success!</strong> You have been signed in successfully!
        </div> --}}
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{session('error_message')}}
        </div>
        @endif

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Categories</h3>
                  <a href="{{route('categories.create')}}" class="btn btn-block btn-primary" style="max-width: 150px; float: right;">Add Category</a>                
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  {{-- <table id="example1" class="table table-bordered table-striped"> --}}
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Status</th>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Section</th>
                            <th>Parent</th>
                            <th>Discount</th>
                            <th>Description</th>
                            <th>URL</th>
                            <th>Meta_Title</th>
                            <th>Meta_Description</th>
                            <th>Meta_Keywords</th>
                            <th>image</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)    
                            <tr>
                                <td>{{ $category->id}}</td>
                                <td>
                                  @if ($category->status==1)
                                    <a type="button" class="updateCategoryStatus" id="category-{{$category->id}}"
                                      category_id="{{$category->id}}" href="javascript:void(0)" title="Inactive"><i class="fas fa-toggle-on" status="Active"></i></a>
                                    @else
                                      <a type="button" class="updateCategoryStatus" id="category-{{$category->id}}"
                                        category_id="{{$category->id}}" href="javascript:void(0)" title="Active"><i class="fas fa-toggle-off" status="Inactive" style="color:red"></i></a>
                                    @endif
                                </td>
                                <td>
                                  <a class="btn btn-info btn-sm" href="{{url('admin/categories/'.$category->id.'/edit')}}" title="Edit"><i class="nav-icon fas fa-pencil-alt"></i></a>
                                  <a class="btn btn-danger btn-sm confirmDelete" href="javascript:void(0)" recordName="Category" record="deleteCategory"  recordId="{{$category->id}}" title="Delete"><i class="far fa-trash-alt"></i></a>
                                  {{-- <form class="confirmDelete" name="Category" action="{{url('admin/categories/'.$category->id)}}" method="POST"> --}}
                                    {{-- @csrf  --}}
                                    {{-- cross-site request forgery
                                        Laravel makes it easy to protect 
                                        your application from cross-site request forgery (CSRF) 
                                        attacks. Cross-site request forgeries are a type of 
                                        malicious exploit whereby unauthorized commands are 
                                        performed on behalf of an authenticated user.  --}}
                                    {{-- @method('delete')
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete"><i class="far fa-trash-alt"></i></button>
                                  </form> --}}
                                </td>
                                <td>{{ $category->name}}</td>
                                <td>{{ $category->section->name}}</td>
                                <td>
                                  @if (!isset($category->parent->name))
                                      {{"Root"}}
                                  @else
                                      {{$category->parent->name}}
                                  @endif
                                </td>
                                <td>{{ $category->discount}}</td>
                                <td>{{ $category->description}}</td>
                                <td>{{ $category->url}}</td>
                                <td>{{ $category->meta_title}}</td>
                                <td>{{ $category->meta_description}}</td>
                                <td>{{ $category->meta_keywords}}</td>
                                <td>
                                  @if (!empty($category->image))
                                  <img src="/images/category_images/{{ $category->image}}" width="50px" alt="category_image" />
                                  @else
                                  {{-- <img src="/images/category_images/{{ $category->image}}" width="50px" alt="category_image" /> --}}
                                  @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                          <th>#</th>
                          <th>Status</th>
                          <th>Action</th>
                          <th>Name</th>
                          <th>Section</th>
                          <th>Parent</th>
                          <th>Discount</th>
                          <th>Description</th>
                          <th>URL</th>
                          <th>Meta_Title</th>
                          <th>Meta_Description</th>
                          <th>Meta_Keywords</th>
                          <th>image</th>
                        </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->


</div>
  <!-- /.content-wrapper -->
@endsection