<div class="form-group">
    <label for="parent_id">Category Level*</label>
    <select class="form-control select2" id="parent_id" style="width: 100%;" name="parent_id" required>
        <option selected="selected" value="0">Root Category</option>
        @if (!empty($getCategries))    
            @foreach ($getCategries as $category)
                <option value="{{$category['id']}}">{{$category['name']}}</option>
                @if (!empty($category['sub_categories']))    
                    @foreach ($category['sub_categories'] as $subCategory)
                        <option value="{{$subCategory['id']}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-&gt;&nbsp;{{$subCategory['name']}}</option>
                    @endforeach
                @endif
            @endforeach
        @endif
    </select>
</div>