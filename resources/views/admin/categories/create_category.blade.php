@extends('layouts.Admin.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Catalogues</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Add Category</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">New Category</h3>
                    <a href="{{route('categories.index')}}" class="btn btn-block btn-primary" style="max-width: 150px; float: right;">All Categories</a>
                    {{-- <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div> --}}
                </div>
                <div>
                    @if (session('success_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('success_message')}}
                        </div>
                        @endif
                        {{-- Successful Message Alert with close and fade effect from controller --}}
                        @if (session('error_message'))
                        {{-- <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>Success!</strong> You have been signed in successfully!
                        </div> --}}
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{session('error_message')}}
                        </div>
                        @endif
                
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <!-- /.card-header -->
                <form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data" id="category_form">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="section_id">Sections*</label>
                                    <select class="form-control select2" style="width: 100%;" name="section_id" id="section_id" required>
                                        <option selected="selected">Select Section</option>
                                        @foreach ($sections as $section)
                                            <option value="{{$section->id}}">{{$section->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="name">Category Name*</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Enter Category Name" required>
                                </div>
                                <div class="form-group">
                                    <label for="discount">Category Discount(%)</label>
                                    <input type="text" class="form-control" id="discount" name="discount" placeholder="Enter Category Discount">
                                </div>
                                <div class="form-group">
                                    <label for="description">Category Description</label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Description" id="description" name="description">{{ old('description') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_description">Meta Description</label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Meta Description" id="meta_description" name="meta_description">{{ old('meta_description') }}</textarea>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parent_id">Category Level*</label>
                                    <select class="form-control select2" id="parent_id" style="width: 100%;" name="parent_id" required>
                                        <option selected="selected" value="0" disabled>Root Category</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="image">Category Image</label>
                                    <div class="input-group">
                                      <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="image" name="image" accept="image/*">
                                        <label class="custom-file-label" for="image">Choose image</label>
                                      </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="url">Category URL*</label>
                                    <input type="text" class="form-control" id="url" name="url" value="{{ old('url') }}" placeholder="Enter Category URL" required>
                                </div>
                                <div class="form-group">
                                    <label for="meta_title">Meta Title</label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Meta Title" id="meta_title" name="meta_title">{{ old('meta_title') }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_keywords">Meta Keywords</label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Meta Keywords" id="meta_keywords" name="meta_keywords">{{ old('meta_keywords') }}</textarea>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </form>
            </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>


</div>
  <!-- /.content-wrapper -->
@endsection