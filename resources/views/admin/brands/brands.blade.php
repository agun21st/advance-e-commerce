@extends('layouts.Admin.admin_layout')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product Brands</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
              <li class="breadcrumb-item active">Product Brands</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- SELECT2 EXAMPLE -->
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Brand List</h3>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add-brand" style="max-width: 150px; float: right;">Add Brand</button>
                </div>
                <div class="card-body">
                    {{-- All Success and Error Messages --}}
                    <div>
                        @if (session('success_message'))
                            {{-- <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Success!</strong> You have been signed in successfully!
                            </div> --}}
                            <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top: 5px;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{session('success_message')}}
                            </div>
                            @endif
                            {{-- Successful Message Alert with close and fade effect from controller --}}
                            @if (session('error_message'))
                            {{-- <div class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Success!</strong> You have been signed in successfully!
                            </div> --}}
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{session('error_message')}}
                            </div>
                            @endif
                    
                            @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    {{-- All Success and Error Messages End --}}
                    {{-- <table id="example1" class="table table-bordered table-striped"> --}}
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($brands as $brand)    
                                <tr>
                                    <td>{{ $brand->id }}</td>
                                    <td>{{ $brand->name}}</td>
                                    <td class="d-flex justify-content-between flex-wrap">
                                        @if ($brand->status==1)
                                            <a type="button" class="updateBrandStatus" id="brand-{{$brand->id}}"
                                            brand_id="{{$brand->id}}" href="javascript:void(0)" title="Inactive"><i class="fas fa-toggle-on" status="Active"></i></a>
                                        @else
                                            <a type="button" class="updateBrandStatus" id="brand-{{$brand->id}}"
                                            brand_id="{{$brand->id}}" href="javascript:void(0)" title="Active"><i class="fas fa-toggle-off" status="Inactive" style="color:red"></i></a>
                                        @endif
                                        <button type="button" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#edit-brand-{{$brand->id}}" title="Edit Brand"><i class="nav-icon fas fa-edit"></i></button>
                                        <a class="confirmDelete btn btn-outline-danger btn-sm" href="javascript:void(0)" recordName="brand" record="deleteBrand"  recordId="{{$brand->id}}" title="Delete Brand"><i class="far fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                                {{-- All Edit Modals --}}
                                <div class="modal fade" id="edit-brand-{{$brand->id}}">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <form action="{{ url('/admin/brands/'.$brand->id) }}" method="post" enctype="multipart/form-data" id="brand_form" >
                                            @csrf
                                            @method('patch')
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Update Brand</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="name">Brand Name*</label>
                                                        <input type="text" class="form-control" id="name" name="name" value="{{ $brand->name }}" placeholder="Enter Brand Name" required>
                                                    </div>
                                                </div>
                                                <div class="modal-footer justify-content-between">
                                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn bg-gradient-primary">Update</button>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                {{-- All Edit Modals End --}}
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{-- All Modals --}}
                <div class="modal fade" id="add-brand">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <form action="{{ route('brands.store') }}" method="post" enctype="multipart/form-data" id="brand_form" >
                            @csrf
                                <div class="modal-header">
                                    <h4 class="modal-title">New Brand</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="name">Brand Name*</label>
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Enter Brand Name" required>
                                    </div>
                                </div>
                                <div class="modal-footer justify-content-between">
                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn bg-gradient-primary">Add Brand</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                {{-- All Modals End --}}
            </div>
          <!-- /.card -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
  <!-- /.content-wrapper -->
@endsection