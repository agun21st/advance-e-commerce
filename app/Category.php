<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'parent_id', 'section_id', 'name', 'image',
        'discount', 'description', 'url', 'meta_title',
        'meta_description', 'meta_keywords', 'status'
    ];

    public function sub_categories()
    {
        return $this->hasMany(Category::class, 'parent_id')->where('status',1);
    }

    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id')->select('id','name')->where('status',1);
    }
    public function parentCategory()
    {
        return $this->belongsTo(Category::class, 'parent_id')->select('id','name')->where('status',1);
    }
}