<?php

namespace App\Http\Controllers\Admin;

use App\Section;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('page','categories');
        $categories = Category::with(['section','parentCategory'])->where('status',1)->get();
        // $categories = json_decode(json_encode($categories),true);
        // echo "<pre>"; print_r($categories); die;
        return view('admin.categories.categories')->with(compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections   =   Section::where('status',1)->get();
        // $categories =   Category::with('sub_categories')->select('id','name')->where('parent_id',0)->where('status',1)->get();
        return view('admin.categories.create_category')->with(compact('sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die;
        $detailsRuls= [
            'name' => 'required|max:255',
            'section_id' => 'required',
            'url' => 'required|max:255',
            'image' => 'image|mimes:jpg,jpeg,png',
        ];
        $customMessage  =   [
            'name.required' => 'Name is required',
        ];
        $this->validate($request, $detailsRuls, $customMessage);
        
        // image update
        $isImageFileHave = 0;
        $newFileName = '';
        $fullPath = '';
        $fileLocation = 'images/category_images/';

        if($request->hasFile('image')){

            
            //! get File Name
            $newImageName = request()->file('image')->getClientOriginalName();
    
            //! remove extension from file name
            // $onlyNewImageName = strtok($newImageName, ".");

            //! get file Extension
            $fileExName = request()->file('image')->getClientOriginalExtension();

            //! better approch for file naming with uniqid() fuction
            // $newFileName = $onlyNewImageName."_".uniqid().".".$fileExName;
            $newFileName = "Category_".uniqid().".".$fileExName;

            //? //* Path with File Name
            $fullPath = $fileLocation.$newFileName;

            // $resizedImage = Image::make($imageFile)->fit(590,708);
            request()->file('image')->move($fileLocation, $newFileName);

            $isImageFileHave = 1;

            //! Image Resize after stored image
            // $image = Image::make($fullPath)->fit(590,708, "top");
            // $image->save();
            $image = Image::make($fullPath)->fit(500, 750, function ($constraint) {
                //$constraint->aspectRatio();
                $constraint->upsize();
            }, "top");
            $image->save();
        }

        $insertData = Category::create([
            'parent_id' => $request->parent_id,
            'section_id' => $request->section_id,
            'name' => $request->name,
            'image' => $isImageFileHave==1?$newFileName:'',
            'discount' => $request->discount==''?0:$request->discount,
            'description' => $request->description,
            'url' => $request->url,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'meta_keywords' => $request->meta_keywords,
        ]);
        
        return redirect()->back()->with('success_message', 'New category added successfully done.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // echo '<pre>'; print_r($id); die;
        $getCategries = Category::with('sub_categories')->where('section_id',$id)->where('parent_id',0)->where('status',1)->get();
        if($getCategries){
            $getCategries = json_decode(json_encode($getCategries),true);
            // echo '<pre>'; print_r($getCategries); die;
            return view('admin.categories.append_categories_level')->with(compact('getCategries'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getCategory = Category::where('id',$id)->first();
        $categories = Category::with('sub_categories')->select('id','name')->where('section_id',$getCategory->section_id)->where('parent_id',0)->where('status',1)->get();
        $sections   = Section::where('status',1)->get();
        if($getCategory){
            // $categories = json_decode(json_encode($categories),true);
            // echo '<pre>'; print_r($categories); die;
            return view('admin.categories.edit_category')->with(compact('getCategory','categories','sections'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo '<pre>'; print_r($id); die;
        $getCategory    =   Category::find($id);

        $detailsRuls= [
            'name' => 'required|max:255',
            'section_id' => 'required',
            'url' => 'required|max:255',
            'image' => 'image|mimes:jpg,jpeg,png',
        ];
        $customMessage  =   [
            'name.required' => 'Name is required',
        ];
        $this->validate($request, $detailsRuls, $customMessage);
        
        // image update
        $imageName = $getCategory->image;
        $isImageFileHave = 0;
        $newFileName = '';
        $fullPath = '';
        $fileLocation = 'images/category_images/';

        if($request->hasFile('image')){

            if($getCategory->image != '' || $getCategory->image != 'null'){
                
                
                $fullPath = $fileLocation.$imageName;
    
                if(\File::exists($fullPath)){
        
                    \File::delete($fullPath);
                }

                //! get File Name
                $newImageName = request()->file('image')->getClientOriginalName();

                //! remove extension from file name
                // $onlyNewImageName = strtok($newImageName, ".");

                //! get file Extension
                $fileExName = request()->file('image')->getClientOriginalExtension();

                //! better approch for file naming with uniqid() fuction
                // $newFileName = $onlyNewImageName."_".uniqid().".".$fileExName;
                $newFileName = "Category_".uniqid().".".$fileExName;

                //? //* Path with File Name
                $fullPath = $fileLocation.$newFileName;

                // $resizedImage = Image::make($imageFile)->fit(590,708);
                request()->file('image')->move($fileLocation, $newFileName);

                $getCategory->update([
                    'image' => $newFileName,
                ]);

                //! Image Resize after stored image
                // $image = Image::make($fullPath)->fit(590,708, "top");
                // $image->save();
                // resize the image so that the largest side fits within the limit; the smaller
                // side will be scaled to maintain the original aspect ratio
                $image = Image::make($fullPath)->fit(300, 300, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top");
                $image->save();


            }else{
                //! get File Name
                $newImageName = request()->file('image')->getClientOriginalName();
        
                //! remove extension from file name
                // $onlyNewImageName = strtok($newImageName, ".");

                //! get file Extension
                $fileExName = request()->file('image')->getClientOriginalExtension();

                //! better approch for file naming with uniqid() fuction
                // $newFileName = $onlyNewImageName."_".uniqid().".".$fileExName;
                $newFileName = "Category_".uniqid().".".$fileExName;

                //? //* Path with File Name
                $fullPath = $fileLocation.$newFileName;

                // $resizedImage = Image::make($imageFile)->fit(590,708);
                request()->file('image')->move($fileLocation, $newFileName);

                $getCategory->update([
                    'image' => $newFileName,
                ]);

                //! Image Resize after stored image
                // $image = Image::make($fullPath)->fit(590,708, "top");
                // $image->save();
                $image = Image::make($fullPath)->fit(500, 750, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top");

                $image->save();
            }
        }

        $getCategory->update([
            'parent_id' => $request->parent_id,
            'section_id' => $request->section_id,
            'name' => $request->name,
            'discount' => $request->discount==''?0:$request->discount,
            'description' => $request->description,
            'url' => $request->url,
            'meta_title' => $request->meta_title,
            'meta_description' => $request->meta_description,
            'meta_keywords' => $request->meta_keywords,
        ]);
        
        return redirect()->back()->with('success_message', 'Category Updated successfully done.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $category = Category::find($id);
        // $name = $category->name;
        // $category->delete();
        // return redirect()->back()->with('success_message', 'Category Deleted successfully.');
    }
    // delete Category 
    public function deleteCategory($id)
    {
        $category = Category::find($id);
        $name = $category->name;
        $category->delete();
        return redirect()->back()->with('success_message', 'Category Deleted successfully.');
    }
    
    // Category Status Update
    public function categoryStatusUpdate(Request $request, $id)
    {
        if($request->ajax())
        {
            $data   =   $request->all();
            $status =   0;
            // echo "<pre>"; print_r($data); die;
            if($request->status=="Active"){
                $status =   0;
            }else{
                $status =   1;
            }

            Category::where('id',$id)->update([
                'status'=>$status,
            ]);
            return response()->json(['status'=>$status]);
        }
    }

    //get Category By Section Id
    public function getCategoryBySectionId($id)
    {
        //? Categories Dropdown Menu Code
        $categories = Category::with('sub_categories')->select('id','name')->where('section_id',$id)->where('parent_id',0)->where('status',1)->get();
        
        $categories_dropdown = "<option value='0' selected>Root Category</option>";

        foreach ($categories as $category)
        {
            $categories_dropdown .= "<option value='".$category->id."'>".$category->name."</option>";

            if (!empty($category->sub_categories))
            {   
                foreach ($category->sub_categories as $subCategory)
                {
                    $categories_dropdown .= "<option value='".$subCategory->id."' disabled>&nbsp;&nbsp;--&nbsp".$subCategory->name."</option>";
                }
            }
        }

        return response()->json(['categories_dropdown'=>$categories_dropdown]);
    }

    //get Category By Section Id for product
    public function getCategoryBySectionIdForProduct($id)
    {
        //? Categories Dropdown Menu Code
        $categories = Category::with('sub_categories')->select('id','name')->where('section_id',$id)->where('parent_id',0)->where('status',1)->get();
        
        $categories_dropdown = "<option selected>Select Category</option>";

        foreach ($categories as $category)
        {
            $categories_dropdown .= "<option value='".$category->id."'>".$category->name."</option>";

            if (!empty($category->sub_categories))
            {   
                foreach ($category->sub_categories as $subCategory)
                {
                    $categories_dropdown .= "<option value='".$subCategory->id."'>&nbsp;&nbsp;--&nbsp".$subCategory->name."</option>";
                }
            }
        }

        return response()->json(['categories_dropdown'=>$categories_dropdown]);
    }

    //Delete Category Image By Section Id
    public function deleteCategoryImage($id)
    {
        //? Categories Dropdown Menu Code
        $getCategory = Category::find($id);

        // image update
        $imageName = $getCategory->image;
        $fullPath = '';
        $fileLocation = 'images/category_images/';
        
        if($getCategory->image != '' || $getCategory->image != null){
                
            
            $fullPath = $fileLocation.$imageName;

            if(\File::exists($fullPath)){
    
                \File::delete($fullPath);
            }

            $getCategory->update([
                'image' => "",
            ]);
        }

        return redirect()->back()->with('success_message', 'Category Image Deleted successfully.');
    }
}
