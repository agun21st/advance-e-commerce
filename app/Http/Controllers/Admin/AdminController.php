<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('page','dashboard');
        return view('admin.admin_dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // Admin settings
    public function settings(){

        Session::put('page','settings');
        $current_user   =  Auth::guard('admin')->user() ;
        return view('admin.admin_settings')->with(compact('current_user'));
    }

    // Login Function
    public function login( Request $request)
    {   
        if($request->isMethod('post'))
        {
            $data = $request->all();
            
            $loginRuls= [
                'email' => 'required|email|max:255',
                'password' => 'required',
            ];
            $customMessage  =   [
                'email.required' => 'Please enter your email',
                'email.email'   => 'Valid email is required',
                'password.required' => 'Please enter the Password'
            ];

            $this->validate($request, $loginRuls, $customMessage);
            
            if(Auth::guard('admin')->attempt(['email'=>$data['email'], 'password'=>$data['password']]))
            {
                return redirect('admin/dashboard');
            }else{
                return redirect()->back()->with('error_message', 'Invalid Email or Password');
            }
        }

        return view('admin.admin_login');
    }

    // Login Function
    public function logout( Request $request)
    {   
        Auth::guard('admin')->logout();

        return redirect('/admin');
    }

    // Admin Current Password Check
    public function checkCurrentPassword(Request $request){
        if(Hash::check($request->cPassword, Auth::guard('admin')->user()->password)){
            return "true";
        }else{
            return "false";
        }
    }

    // Update Admin Current Password
    public function updateCurrentPassword(Request $request){
        
        request()->validate([
            'password' => ['required', 'string', 'min:3', 'max:30', 'confirmed'],
        ]);

        // if current password match then
        if(Hash::check($request->current_password, Auth::guard('admin')->user()->password)){
            
            Admin::where('id',Auth::guard('admin')->user()->id)->update([
                'password'=>bcrypt($request->password),
            ]);
            return redirect()->back()->with('success_message', 'Password update successfully done.');
        }else{
            return redirect()->back()->with('error_password_update', 'Current Password do not matched');
        }
    }

    // Update Admin Details
    public function updateAdminDetails(Request $request)
    {
        
        Session::put('page','update-admin-details');

        if($request->isMethod('post'))
        {
            $detailsRuls= [
                'name' => 'required|max:255|regex:/^[a-zA-ZÑñ\s]+$/',
                'mobile' => 'regex:/^[-0-9\+]+$/',
                'image' => 'image|mimes:jpg,jpeg,png',
            ];
            $customMessage  =   [
                'name.required' => 'Name is required',
            ];
            $this->validate($request, $detailsRuls, $customMessage);

            Admin::where('id',Auth::guard('admin')->user()->id)->update([
                'name'=> $request->name,
                'mobile' => $request->mobile,
            ]);

            // image update
            $fullPath = '';
            $fileLocation = 'images/admin_images/';

            if(request()->hasFile('image')){

                if(Auth::guard('admin')->user()->image != '' || Auth::guard('admin')->user()->image != null){
                
                    $imageName = Auth::guard('admin')->user()->image;
                    $fullPath = $fileLocation.$imageName;
        
                    if(\File::exists($fullPath)){
            
                        \File::delete($fullPath);
                    }
    
                    //! get File Name
                    $newImageName = request()->file('image')->getClientOriginalName();
    
                    //! remove extension from file name
                    // $onlyNewImageName = strtok($newImageName, ".");
    
                    //! get file Extension
                    $fileExName = request()->file('image')->getClientOriginalExtension();
    
                    //! better approch for file naming with uniqid() fuction
                    // $newFileName = $onlyNewImageName."_".uniqid().".".$fileExName;
                    $newFileName = "Admin_".uniqid().".".$fileExName;
    
                    //? //* Path with File Name
                    $fullPath = $fileLocation.$newFileName;
    
                    // $resizedImage = Image::make($imageFile)->fit(590,708);
                    request()->file('image')->move($fileLocation, $newFileName);

                    Admin::where('id',Auth::guard('admin')->user()->id)->update([
                        'image' => $newFileName,
                    ]);
                    //! Image Resize after stored image
                    // $image = Image::make($fullPath)->fit(590,708, "top");
                    // $image->save();
                    // resize the image so that the largest side fits within the limit; the smaller
                    // side will be scaled to maintain the original aspect ratio
                    $image = Image::make($fullPath)->fit(300, 300, function ($constraint) {
                        //$constraint->aspectRatio();
                        $constraint->upsize();
                    }, "top");
                    $image->save();
    
    
                }else{
                    //! get File Name
                    $newImageName = request()->file('image')->getClientOriginalName();
    
                    //! remove extension from file name
                    $onlyNewImageName = strtok($newImageName, ".");
    
                    //! get file Extension
                    $fileExName = request()->file('image')->getClientOriginalExtension();
    
                    //! better approch for file naming with uniqid() fuction
                    $newFileName = $onlyNewImageName."_".uniqid().".".$fileExName;
    
                    //? //* Path with File Name
                    $fullPath = $fileLocation.$newFileName;
    
                    // $resizedImage = Image::make($imageFile)->fit(590,708);
                    request()->file('image')->move($fileLocation, $newFileName);

                    Admin::where('id',Auth::guard('admin')->user()->id)->update([
                        'image' => $newFileName,
                    ]);
    
                    //! Image Resize after stored image
                    // $image = Image::make($fullPath)->fit(590,708, "top");
                    // $image->save();
                    $image = Image::make($fullPath)->fit(300, 300, function ($constraint) {
                        //$constraint->aspectRatio();
                        $constraint->upsize();
                    }, "top");
                    $image->save();
                }
            }

            return redirect()->back()->with('success_message', 'Details update successfully done.');

        }else{
            
            $current_user   =  Auth::guard('admin')->user() ;
            return view('admin.update_admin_details')->with(compact('current_user'));
        }

    }

    //! Validation formate example
    // 'fname' => 'required|max:255|regex:/^[a-zA-ZÑñ\s]+$/',
    // 'lname' => 'required|max:255|regex:/^[a-zA-ZÑñ\s]+$/',
    // 'mname' => 'max:255|alpha',
    // 'file' => 'image|mimes:jpg,jpeg,png',
    // 'contact_no' => 'regex:/^[-0-9\+]+$/',
    // 'date_of_birth' => 'required|date_format:Y-m-d',
    // 'school_id' => 'required|exists:schools,id',
    // 'degree_id' => 'required|exists:degrees,id',
}
