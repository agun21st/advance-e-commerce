<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\Product;
use App\Section;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Session;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('page','products');
        $products = Product::with(['section','brand','category'])->where('status',1)->get();
        // $products = json_decode(json_encode($products),true);
        // echo "<pre>"; print_r($products); die;
        return view('admin.products.products')->with(compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sections       =   Section::where('status',1)->get();
        $brands         =   Brand::where('status',1)->get();
        $categories     =   Category::where('status',1)->get();

        // Filter Arrays
        $fabrics    =   array('Cotton', 'polyester', 'Wool');
        $sleeves    =   array('Full Sleeve', 'Half Sleeve', 'Short Sleeves', 'Sleeveless');
        $patterns   =   array('Checked', 'Plain', 'Printed', 'Self', 'Solid');
        $fits       =   array('Regular', 'Slim');
        $occasions  =   array('Casual', 'Formal');
        // $categories =   Category::with('sub_categories')->select('id','name')->where('parent_id',0)->where('status',1)->get();
        return view('admin.products.add_product')->with(compact(
            'sections',
            'brands',
            'categories',
            'fabrics',
            'sleeves',
            'patterns',
            'fits',
            'occasions'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo "<pre>"; print_r($request->all()); die;
        $detailsRules= [
            'section_id_for_product' => 'required',
            'brand_id_for_product' => 'required',
            'category_id_for_product' => 'required',
            'name' => 'required|max:255',
            'color' => 'required|max:255',
            'code' => 'required|alpha_num',
            'price' => 'required|numeric',
            'main_image' => 'image|mimes:jpg,jpeg,png',
            'video' => 'mimes:mp4,qt | max:10000',
        ];
        $customMessage  =   [
            'name.required' => 'Name is required',
        ];
        $this->validate($request, $detailsRules, $customMessage);

        // image update
        $isImageFileHave = 0;
        $newFileName = '';
        $fullPath = '';
        $fileLocation = 'images/product_images/large/';
        $mediumFileLocation = 'images/product_images/medium/';
        $smallFileLocation = 'images/product_images/small/';

        if($request->hasFile('main_image')){

            $getFile    =   request()->file('main_image');

            //! get file Extension
            $fileExName = request()->file('main_image')->getClientOriginalExtension();

            //! better approch for file naming with uniqid() fuction
            $newFileName = "Product_".uniqid().".".$fileExName;

            //? //* Path with File Name
            $fullPath = $fileLocation.$newFileName;
            $mediumFullPath = $mediumFileLocation.$newFileName;
            $smallFullPath = $smallFileLocation.$newFileName;

            $isImageFileHave = 1;

            //! Image Resize after stored image
            // Save Large Image
            Image::make($getFile)->fit(720, 1080, function ($constraint) {
                //$constraint->aspectRatio();
                $constraint->upsize();
            }, "top")->save($fullPath);
            // save medium image
            Image::make($getFile)->fit(360, 540, function ($constraint) {
                //$constraint->aspectRatio();
                $constraint->upsize();
            }, "top")->save($mediumFullPath);
            // save small image
            Image::make($getFile)->fit(180, 270, function ($constraint) {
                //$constraint->aspectRatio();
                $constraint->upsize();
            }, "top")->save($smallFullPath);
        }

        // Video upload
        $isVideoFileHave = 0;
        $newVideoFileName = '';
        $videoFileLocation = 'videos/product_videos/';

        if($request->hasFile('video')){

            //! get file Extension
            $videoFileExName = request()->file('video')->getClientOriginalExtension();

            //! better approch for file naming with uniqid() fuction
            // $newVideoFileName = $onlyNewVideoName."_".uniqid().".".$videoFileExName;
            $newVideoFileName = "Product_Video_".uniqid().".".$videoFileExName;

            // $resizedImage = Image::make($imageFile)->fit(590,708);
            request()->file('video')->move($videoFileLocation, $newVideoFileName);

            $isVideoFileHave = 1;
        }


        $storeProduct = Product::create([
            'section_id'    =>   $request->section_id_for_product,
            'brand_id'      =>   $request->brand_id_for_product,
            'category_id'   =>   $request->category_id_for_product,
            'name'          =>   $request->name,
            'code'          =>   $request->code,
            'color'         =>   $request->color,
            'price'         =>   $request->price,
            'discount'      =>   $request->discount==''?0:$request->discount,
            'weight'        =>   $request->weight,
            'description'   =>   $request->description,
            'main_image'    =>   $isImageFileHave==1?$newFileName:'',
            'video'         =>   $isVideoFileHave==1?$newVideoFileName:'',
            'wash_care'     =>   $request->wash_care,
            'fabric'        =>   $request->fabric,
            'pattern'       =>   $request->pattern,
            'sleeve'        =>   $request->sleeve,
            'fit'           =>   $request->fit,
            'occasion'      =>   $request->occasion,
            'meta_title'    =>   $request->meta_title,
            'meta_description'  =>   $request->meta_description,
            'meta_keywords' =>   $request->meta_keywords,
            'is_featured'   =>   $request->is_featured=='on'?'Yes':'No',
        ]);

        return redirect()->back()->with('success_message', 'Product Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product    = Product::where('id',$id)->first();
        $sections   = Section::where('status',1)->get();
        $brands     = Brand::where('status',1)->get();
        $categories = Category::with('sub_categories')->select('id','name')->where('section_id',$product->section_id)->where('parent_id',0)->where('status',1)->get();
        // Filter Arrays
        $fabrics    =   array('Cotton', 'polyester', 'Wool');
        $sleeves    =   array('Full Sleeve', 'Half Sleeve', 'Short Sleeves', 'Sleeveless');
        $patterns   =   array('Checked', 'Plain', 'Printed', 'Self', 'Solid');
        $fits       =   array('Regular', 'Slim');
        $occasions  =   array('Casual', 'Formal');

        if($product){
            // $product = json_decode(json_encode($product),true);
            // echo '<pre>'; print_r($product); die;
            return view('admin.products.edit_product')->with(compact('product','categories','sections','brands','fabrics','sleeves', 'patterns', 'fits', 'occasions'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $products = json_decode(json_encode($products),true);
        // echo "<pre>"; print_r($request->all()); die;
        $getProduct    =   Product::find($id);

        $detailsRules= [
            'section_id_for_product' => 'required',
            'brand_id_for_product' => 'required',
            'category_id_for_product' => 'required',
            'name' => 'required|max:255',
            'color' => 'required|max:255',
            'code' => 'required|alpha_num',
            'price' => 'required|numeric',
            'main_image' => 'image|mimes:jpg,jpeg,png',
            'video' => 'mimes:mp4,qt | max:10000',
        ];
        $customMessage  =   [
            'name.required' => 'Name is required',
        ];
        $this->validate($request, $detailsRules, $customMessage);

        // image update
        $imageName = $getProduct->main_image;
        $isImageFileHave = 0;
        $newFileName = '';
        $fullPath = '';
        $fileLocation = 'images/product_images/large/';
        $mediumFileLocation = 'images/product_images/medium/';
        $smallFileLocation = 'images/product_images/small/';

        if($request->hasFile('main_image')){

            if($getProduct->main_image != '' || $getProduct->main_image != 'null'){
                
                $fullPath = $fileLocation.$imageName;
                $fullPathMedium = $mediumFileLocation.$imageName;
                $fullPathSmall = $smallFileLocation.$imageName;
    
                if(\File::exists($fullPath)){
        
                    \File::delete($fullPath);
                }
                if(\File::exists($fullPathMedium)){
    
                    \File::delete($fullPathMedium);
                }
                if(\File::exists($fullPathSmall)){
        
                    \File::delete($fullPathSmall);
                }

                $getFile    =   request()->file('main_image');

                //! get file Extension
                $fileExName = request()->file('main_image')->getClientOriginalExtension();

                //! better approch for file naming with uniqid() fuction
                $newFileName = "Product_".uniqid().".".$fileExName;

                //? //* Path with File Name
                $fullPath = $fileLocation.$newFileName;
                $mediumFullPath = $mediumFileLocation.$newFileName;
                $smallFullPath = $smallFileLocation.$newFileName;

                $isImageFileHave = 1;

                //! Image Resize after stored image
                // Save Large Image
                Image::make($getFile)->fit(720, 1080, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($fullPath);
                // save medium image
                Image::make($getFile)->fit(360, 540, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($mediumFullPath);
                // save small image
                Image::make($getFile)->fit(180, 270, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($smallFullPath);


            }else{

                $getFile    =   request()->file('main_image');

                //! get file Extension
                $fileExName = request()->file('main_image')->getClientOriginalExtension();

                //! better approch for file naming with uniqid() fuction
                $newFileName = "Product_".uniqid().".".$fileExName;

                //? //* Path with File Name
                $fullPath = $fileLocation.$newFileName;
                $mediumFullPath = $mediumFileLocation.$newFileName;
                $smallFullPath = $smallFileLocation.$newFileName;

                $isImageFileHave = 1;

                //! Image Resize after stored image
                // Save Large Image
                Image::make($getFile)->fit(720, 1080, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($fullPath);
                // save medium image
                Image::make($getFile)->fit(360, 540, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($mediumFullPath);
                // save small image
                Image::make($getFile)->fit(180, 270, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($smallFullPath);

            }
        }

        // Video upload
        $videoName = $getProduct->video;
        $isVideoFileHave = 0;
        $newVideoFileName = '';
        $videoFileLocation = 'videos/product_videos/';

        if($request->hasFile('video')){

            if($getProduct->video != '' || $getProduct->video != 'null'){
                
                $fullPath = $videoFileLocation.$videoName;
    
                if(\File::exists($fullPath)){
        
                    \File::delete($fullPath);
                }
    
                //! get file Extension
                $videoFileExName = request()->file('video')->getClientOriginalExtension();

                //! better approch for file naming with uniqid() fuction
                // $newVideoFileName = $onlyNewVideoName."_".uniqid().".".$videoFileExName;
                $newVideoFileName = "Product_Video_".uniqid().".".$videoFileExName;

                // $resizedImage = Image::make($imageFile)->fit(590,708);
                request()->file('video')->move($videoFileLocation, $newVideoFileName);

                $isVideoFileHave = 1;
                
            }else{

                //! get file Extension
                $videoFileExName = request()->file('video')->getClientOriginalExtension();

                //! better approch for file naming with uniqid() fuction
                // $newVideoFileName = $onlyNewVideoName."_".uniqid().".".$videoFileExName;
                $newVideoFileName = "Product_Video_".uniqid().".".$videoFileExName;

                // $resizedImage = Image::make($imageFile)->fit(590,708);
                request()->file('video')->move($videoFileLocation, $newVideoFileName);

                $isVideoFileHave = 1;
            }
        }


        if(($imageName != '' || $imageName=!'null') && $isImageFileHave==0){
            $newFileName =  $imageName;
            $isImageFileHave    =   1;
        }
        if(($videoName != '' || $videoName=!'null') && $isVideoFileHave==0){
            $newVideoFileName =  $videoName;
            $isVideoFileHave    =   1;
        }

        $getProduct->update([
            'section_id'    =>   $request->section_id_for_product,
            'brand_id'    =>   $request->brand_id_for_product,
            'category_id'   =>   $request->category_id_for_product,
            'name'          =>   $request->name,
            'code'          =>   $request->code,
            'color'         =>   $request->color,
            'price'         =>   $request->price,
            'discount'      =>   $request->discount==''?0:$request->discount,
            'weight'        =>   $request->weight,
            'description'   =>   $request->description,
            'main_image'    =>   $isImageFileHave==1?$newFileName:'',
            'video'         =>   $isVideoFileHave==1?$newVideoFileName:'',
            'wash_care'     =>   $request->wash_care,
            'fabric'        =>   $request->fabric,
            'pattern'       =>   $request->pattern,
            'sleeve'        =>   $request->sleeve,
            'fit'           =>   $request->fit,
            'occasion'      =>   $request->occasion,
            'meta_title'    =>   $request->meta_title,
            'meta_description'  =>   $request->meta_description,
            'meta_keywords' =>   $request->meta_keywords,
            'is_featured'   =>   $request->is_featured=='on'?'Yes':'No',
        ]);

        return redirect()->back()->with('success_message', 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // delete Product 
    public function deleteProduct($id)
    {
        $getProduct = Product::find($id);

        //Delete image
        $imageName = $getProduct->main_image;
        $fullPath = '';
        $fileLocation = 'images/Product_images/large/';
        $fileLocationMedium = 'images/Product_images/medium/';
        $fileLocationSmall = 'images/Product_images/small/';
        
        if($getProduct->main_image != '' || $getProduct->main_image != null){
                
            $fullPath = $fileLocation.$imageName;
            $fullPathMedium = $fileLocationMedium.$imageName;
            $fullPathSmall = $fileLocationSmall.$imageName;

            if(\File::exists($fullPath)){
    
                \File::delete($fullPath);
            }
            if(\File::exists($fullPathMedium)){
    
                \File::delete($fullPathMedium);
            }
            if(\File::exists($fullPathSmall)){
    
                \File::delete($fullPathSmall);
            }
        }

        //Delete Video
        $videoName = $getProduct->video;
        $videoFullPath = '';
        $videoFileLocation = 'videos/Product_videos/';
        
        if($getProduct->video != '' || $getProduct->video != null){

            $videoFullPath = $videoFileLocation.$videoName;

            if(\File::exists($videoFullPath)){
    
                \File::delete($videoFullPath);
            }
        }

        $getProduct->delete();
        return redirect()->back()->with('success_message', 'Product Deleted successfully.');
    }
    
    // Product Status Update
    public function productStatusUpdate(Request $request, $id)
    {
        if($request->ajax())
        {
            $data   =   $request->all();
            $status =   0;
            // echo "<pre>"; print_r($data); die;
            if($request->status=="Active"){
                $status =   0;
            }else{
                $status =   1;
            }

            Product::where('id',$id)->update([
                'status'=>$status,
            ]);
            return response()->json(['status'=>$status]);
        }
    }

    //get Product By Section Id
    public function getCategoriesBySectionId($id)
    {
        //? Categories Dropdown Menu Code
        $categories = Category::with('sub_categories')->select('id','name')->where('section_id',$id)->where('parent_id',0)->where('status',1)->get();
        
        $categories_dropdown = "<option value='0' selected>Root Product</option>";

        foreach ($categories as $category)
        {
            $categories_dropdown .= "<option value='".$category->id."'>".$category->name."</option>";

            if (!empty($category->sub_categories))
            {   
                foreach ($category->sub_categories as $subProduct)
                {
                    $categories_dropdown .= "<option value='".$subProduct->id."' disabled>&nbsp;&nbsp;--&nbsp".$subProduct->name."</option>";
                }
            }
        }

        return response()->json(['categories_dropdown'=>$categories_dropdown]);
    }

    //Delete Product Image
    public function deleteProductMainImage($id)
    {
        //? Categories Dropdown Menu Code
        $getProduct = Product::find($id);

        // image update
        $fullPath = '';
        $fileLocation = 'images/Product_images/large/';
        $fileLocationMedium = 'images/Product_images/medium/';
        $fileLocationSmall = 'images/Product_images/small/';
        
        if($getProduct->main_image != '' || $getProduct->main_image != 'null'){
                
            $imageName = $getProduct->main_image;
            $fullPath = $fileLocation.$imageName;
            $fullPathMedium = $fileLocationMedium.$imageName;
            $fullPathSmall = $fileLocationSmall.$imageName;

            if(\File::exists($fullPath)){
    
                \File::delete($fullPath);
            }
            if(\File::exists($fullPathMedium)){
    
                \File::delete($fullPathMedium);
            }
            if(\File::exists($fullPathSmall)){
    
                \File::delete($fullPathSmall);
            }

            $getProduct->update([
                'main_image' => "",
            ]);
        }

        return redirect()->back()->with('success_message', 'Product Image Deleted successfully.');
    }

    //Delete Product Video 
    public function deleteProductVideo($id)
    {
        //? Categories Dropdown Menu Code
        $getProduct = Product::find($id);

        // image update
        $fullPath = '';
        $fileLocation = 'videos/Product_videos/';
        
        if($getProduct->video != '' || $getProduct->video != 'null'){
                
            $videoName = $getProduct->video;
            $fullPath = $fileLocation.$videoName;

            if(\File::exists($fullPath)){
    
                \File::delete($fullPath);
            }

            $getProduct->update([
                'video' => "",
            ]);
        }

        return redirect()->back()->with('success_message', 'Product Video Deleted successfully.');
    }
}
