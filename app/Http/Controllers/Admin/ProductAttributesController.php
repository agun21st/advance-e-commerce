<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductAttribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductAttributesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $getProduct = json_decode(json_encode($getProduct),true);
        // echo "<pre>"; print_r($request->all()); die;

        // Data Validation
        $detailsRules= [
            'stock' => 'required|max:255',
            'price' => 'required',
            'stock' => 'required',
            'sku' => 'required|max:255',
        ];
        $customMessage  =   [
            'size.required' => 'Size is required in word (Small/Medium/Large/Extra Large)',
        ];
        $this->validate($request, $detailsRules, $customMessage);

        $allData    =   $request->all();
        foreach ($allData['sku'] as $key => $value) {
            if(!empty($value))
            {
                //! SKU already exists or not?
                $checkSKU = ProductAttribute::where('sku',$value)->count();
                if($checkSKU>0)
                {
                    return redirect()->back()->with('error_message', 'Product SKU already added, Please use another SKU.');
                }

                //! Size already exists or not?
                $checkSize = ProductAttribute::where('product_id', $allData['product_id'])->where('size',$allData['size'][$key])->count();
                if($checkSize>0)
                {
                    return redirect()->back()->with('error_message', 'Product Size already added, Please use another Size.');
                }

                $attribute  =  new ProductAttribute;
                $attribute->product_id  =   $allData['product_id'];
                $attribute->sku         =   $value;
                $attribute->size        =   $allData['size'][$key];
                $attribute->price       =   $allData['price'][$key];
                $attribute->stock       =   $allData['stock'][$key];

                $attribute->save();
            }
        }

        return redirect()->back()->with('success_message', 'Product Attributes Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getProduct     =   Product::with('attributes')->find($id);
        // $getAttributes  =   ProductAttribute::where('product_id', $id)->where('status',1)->get();
        // $getProduct = json_decode(json_encode($getProduct),true);
        // echo "<pre>"; print_r($getProduct); die;
        return view('admin.products.attributes.add_attributes')->with(compact('getProduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // $getAttributes     =   ProductAttribute::where('product_id',$id)->get();
        // $getAttributes = json_decode(json_encode($getAttributes),true);
        // echo "<pre>"; print_r($request->all()); die;
        $data = $request->all();

        foreach ($data['attributeId'] as $key => $attrId) {
            if(!empty($attrId))
            {
                ProductAttribute::where([ 'product_id'=>$id, 'id'=>$attrId ])->update([
                    'price' => $data['price'][$key],
                    'stock' => $data['stock'][$key],
                ]);
            }
        }

        return redirect()->back()->with('success_message', 'Product Price and Stock Updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Attribute Status Update
    public function updateAttributeStatus(Request $request, $id)
    {
        if($request->ajax())
        {
            $data   =   $request->all();
            $status =   0;
            // echo "<pre>"; print_r($data); die;
            if($request->status=="Active"){
                $status =   0;
            }else{
                $status =   1;
            }

            ProductAttribute::where('id',$id)->update([
                'status'=>$status,
            ]);
            return response()->json(['status'=>$status]);
        }
    }

    // delete attribute 
    public function deleteAttribute($id)
    {
        $getAttribute = ProductAttribute::find($id);

        $getAttribute->delete();
        return redirect()->back()->with('success_message', 'Attribute Deleted successfully.');
    }
}
