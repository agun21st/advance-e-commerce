<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ProductImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('productImage'))
        {
            $images     =   $request->file('productImage');

            // image update
            $newFileName        = '';
            $fullPath           = '';
            $fileLocation       = 'images/product_images/large/';
            $mediumFileLocation = 'images/product_images/medium/';
            $smallFileLocation  = 'images/product_images/small/';

            // echo "<pre>"; print_r($images); die;
            foreach ($images as $key => $image) {
                
                $productImage = new ProductImage;

                $getFile    =   $image;

                //! get file Extension
                $fileExName = $getFile->getClientOriginalExtension();

                //! better way for file naming with uniqid() fuction
                $newFileName = "Product_".uniqid().".".$fileExName;

                //? //* Path with File Name
                $fullPath       = $fileLocation.$newFileName;
                $mediumFullPath = $mediumFileLocation.$newFileName;
                $smallFullPath  = $smallFileLocation.$newFileName;

                //! Image Resize after stored image
                // Save Large Image
                Image::make($getFile)->fit(720, 1080, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($fullPath);
                // save medium image
                Image::make($getFile)->fit(360, 540, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($mediumFullPath);
                // save small image
                Image::make($getFile)->fit(180, 270, function ($constraint) {
                    //$constraint->aspectRatio();
                    $constraint->upsize();
                }, "top")->save($smallFullPath);

                $productImage->image = $newFileName;
                $productImage->product_id = $request->product_id;
                $productImage->save();
                
            }

            return redirect()->back()->with('success_message', 'Product images Added successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $getProductImages     =   Product::with('images')->find($id);
        // $getAttributes  =   ProductAttribute::where('product_id', $id)->where('status',1)->get();
        // $getProductImages = json_decode(json_encode($getProductImages),true);
        // echo "<pre>"; print_r($getProductImages); die;
        return view('admin.products.images.add_images')->with(compact('getProductImages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Attribute Status Update
    public function updateProductImageStatus(Request $request, $id)
    {
        if($request->ajax())
        {
            $data   =   $request->all();
            $status =   0;
            // echo "<pre>"; print_r($data); die;
            if($request->status=="Active"){
                $status =   0;
            }else{
                $status =   1;
            }

            ProductImage::where('id',$id)->update([
                'status'=>$status,
            ]);
            return response()->json(['status'=>$status]);
        }
    }

    // delete Product Image 
    public function deleteProductImage($id)
    {
        $getProductImage = ProductImage::find($id);
        //Delete image
        $imageName = $getProductImage->image;
        $fullPath = '';
        $fileLocation = 'images/Product_images/large/';
        $fileLocationMedium = 'images/Product_images/medium/';
        $fileLocationSmall = 'images/Product_images/small/';
        
        if($getProductImage->image != '' || $getProductImage->image != 'null'){
                
            $fullPath = $fileLocation.$imageName;
            $fullPathMedium = $fileLocationMedium.$imageName;
            $fullPathSmall = $fileLocationSmall.$imageName;

            if(\File::exists($fullPath)){
    
                \File::delete($fullPath);
            }
            if(\File::exists($fullPathMedium)){
    
                \File::delete($fullPathMedium);
            }
            if(\File::exists($fullPathSmall)){
    
                \File::delete($fullPathSmall);
            }
        }

        $getProductImage->delete();

        return redirect()->back()->with('success_message', 'Product Image Deleted successfully.');
    }
}
