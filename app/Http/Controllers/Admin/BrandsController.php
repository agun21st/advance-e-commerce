<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class BrandsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session::put('page','brands');
        $brands = Brand::all();
        return view('admin.brands.brands')->with(compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // echo '<pre>'; print_r($request->all()); die;
        $detailsRules= [
            'name' => 'required|max:255|unique:brands',
        ];
        $customMessage  =   [
            'name.required' => 'Brand Name is required',
        ];
        $this->validate($request, $detailsRules, $customMessage);

        $storeBrand = Brand::create([
            'name' => $request->name,
        ]);
        if($storeBrand){
            return redirect()->back()->with('success_message', 'Brand Added successfully.');
        }else{
            return redirect()->back()->with('error_message', 'Brand Can not added, Please check brand table.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo '<pre>'; print_r($id); die;
        $detailsRules= [
            'name' => ['required', 'string', 'max:255', 'unique:brands,name,'.$id],
        ];
        $customMessage  =   [
            'name.required' => 'Unique Brand Name is required',
        ];
        $this->validate($request, $detailsRules, $customMessage);

        $getBrand = Brand::find($id);
        if($getBrand)
        {
            $getBrand->update([
                'name' => $request->name,
            ]);
            return redirect()->back()->with('success_message', 'Brand Updated successfully.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // delete Brand 
    public function deleteBrand($id)
    {
        $brand = Brand::find($id);
        $brand->delete();
        return redirect()->back()->with('success_message', 'Brand Deleted successfully.');
    }

    // Brand Status Update
    public function updateBrandStatus(Request $request, $id)
    {
        if($request->ajax())
        {
            $data   =   $request->all();
            $status =   0;
            // echo "<pre>"; print_r($data); die;
            if($request->status=="Active"){
                $status =   0;
            }else{
                $status =   1;
            }

            Brand::where('id',$id)->update([
                'status'=>$status,
            ]);
            return response()->json(['status'=>$status]);
        }
    }
}
