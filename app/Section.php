<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'status',
    ];

    public function categories(){
        return $this->hasMany(Category::class, 'section_id')->select('id','name')
            ->where(['parent_id'=>0, 'status'=>1])->with('sub_categories');
    }
}
