<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable =   [
        'section_id', 'brand_id', 'category_id', 'name', 'code', 'color', 'price',
        'discount', 'weight', 'main_image', 'video', 'description',
        'wash_care', 'fabric', 'pattern', 'sleeve', 'fit', 'occasion',
        'meta_title', 'meta_description', 'meta_keywords', 'is_featured',
        'status'
    ];
    
    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id')->select('id','name')->where('status',1);
    }
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id')->select('id','name');
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->select('id','name')->where('status',1);
    }
    public function attributes()
    {
        return $this->hasMany(ProductAttribute::class, 'product_id');
    }
    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_id');
    }
}
