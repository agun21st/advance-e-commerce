<?php

namespace Database\Seeders;

use App\ProductAttribute;
use Illuminate\Database\Seeder;

class ProductAttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productAttributesData   =   [
            [
                'product_id'    =>  1,
                'size'          =>  'Small',
                'price'         =>  1200,
                'stock'         =>  10,
                'sku'           =>  'BCT001-S',
            ],
            [
                'product_id'    =>  1,
                'size'          =>  'Medium',
                'price'         =>  1350,
                'stock'         =>  20,
                'sku'           =>  'BCT001-M',
            ],
            [
                'product_id'    =>  1,
                'size'          =>  'Large',
                'price'         =>  1450,
                'stock'         =>  10,
                'sku'           =>  'BCT001-L',
            ],
        ];

        ProductAttribute::insert($productAttributesData);
    }
}
