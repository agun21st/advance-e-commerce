<?php

namespace Database\Seeders;

use App\Section;
use Illuminate\Database\Seeder;
use DB;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->delete();

        $sectionItems   =   [

            ["name"=>"Men"],
            ["name"=>"Women"],
            ["name"=>"Kids"],
        ];

        Section::insert($sectionItems);
    }
}
