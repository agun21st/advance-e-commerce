<?php

namespace Database\Seeders;

use App\ProductImage;
use Illuminate\Database\Seeder;
use DB;

class ProductImagesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_images')->delete();

        $productImages   =   [

            [
                "product_id"=>1,
                "image"=>"Product_608d048791241.jpg"
            ],
        ];

        ProductImage::insert($productImages);
    }
}
