<?php

namespace Database\Seeders;

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productRecords =   [

            [
                'category_id'   =>  11,
                'section_id'    =>  1,
                'name'          =>  'Blue Casual T-Shirt',
                'code'          =>  'BT001',
                'color'         =>  'Blue',
                'price'         =>  '1500',
                'discount'      =>  10,
                'weight'        =>  200,
                'main_image'    =>  '',
                'video'         =>  '',
                'description'   =>  'Test Product',
                'wash_care'     =>  '',
                'fabric'        => '',
                'pattern'       =>  '',
                'sleeve'        =>  '',
                'fit'           =>  '',
                'occasion'      =>  '',
                'meta_title'    =>  '',
                'meta_description'  =>  '',
                'meta_keywords'     =>  '',
                'is_featured'   =>  'No',
            ],
            [
                'category_id'   =>  11,
                'section_id'    =>  1,
                'name'          =>  'Red Casual T-Shirt',
                'code'          =>  'RT001',
                'color'         =>  'Red',
                'price'         =>  '1000',
                'discount'      =>  5,
                'weight'        =>  300,
                'main_image'    =>  '',
                'video'         =>  '',
                'description'   =>  'Test Product-2',
                'wash_care'     =>  '',
                'fabric'        => '',
                'pattern'       =>  '',
                'sleeve'        =>  '',
                'fit'           =>  '',
                'occasion'      =>  '',
                'meta_title'    =>  '',
                'meta_description'  =>  '',
                'meta_keywords'     =>  '',
                'is_featured'   =>  'Yes',
            ],
        ];

        Product::insert($productRecords);
    }
}