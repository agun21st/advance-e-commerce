<?php

namespace Database\Seeders;

use App\Category;
use Illuminate\Database\Seeder;
use DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        $categoriesRecord   =   [

            [
                'parent_id'     =>  0,
                'section_id'    =>  1,
                'name'          =>  "T-Shirts",
                'image'         =>  "",
                'discount'      =>  0,
                'description'   =>  "",
                'url'           =>  "t-shirts",
                'meta_title'    =>  "",
                'meta_description'  =>  "",
                'meta_keywords' =>  "",
                'status'        =>  1,

            ],
            [
                'parent_id'     =>  1,
                'section_id'    =>  1,
                'name'          =>  "Casual T-Shirts",
                'image'         =>  "",
                'discount'      =>  0,
                'description'   =>  "",
                'url'           =>  "casual-t-shirts",
                'meta_title'    =>  "",
                'meta_description'  =>  "",
                'meta_keywords' =>  "",
                'status'        =>  1,

            ],
        ];

        Category::insert($categoriesRecord);
    }
}
