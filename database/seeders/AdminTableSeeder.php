<?php

namespace Database\Seeders;

use App\Admin;
use Illuminate\Database\Seeder;
use DB;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();

        $adminRecords = [

            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'agun21st@outlook.com',
                'password' => '$2y$10$PJe.ETT04hgIQgSaKULlQ.VnXI4bs7p.F/w2.y3aGGFnL586eG/lW',
                'type' => 'super_admin',
                'mobile' => '01712834621',
                'image' => '',
                'status' => 1
            ],
            [
                'id' => 2,
                'name' => 'Sub-Admin',
                'email' => 'subadmin@hafezrice.com',
                'password' => '$2y$10$PJe.ETT04hgIQgSaKULlQ.VnXI4bs7p.F/w2.y3aGGFnL586eG/lW',
                'type' => 'sub_admin',
                'mobile' => '01912834621',
                'image' => '',
                'status' => 0
            ],
        ];

        foreach ($adminRecords as $key => $record) {
            Admin::create($record);
        }
    }
}
