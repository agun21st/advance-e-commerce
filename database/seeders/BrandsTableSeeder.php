<?php

namespace Database\Seeders;

use App\Brand;
use Illuminate\Database\Seeder;
use DB;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->delete();

        $brandRecord = [
            [
                'name'=>'Arrow',
            ],
            [
                'name'=>'Gap',
            ],
            [
                'name'=>'Lee',
            ],
            [
                'name'=>'Monte Carlo',
            ],
            [
                'name'=>'Peter England',
            ],
        ];

        Brand::insert($brandRecord);
    }
}
