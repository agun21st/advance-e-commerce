<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->namespace('Admin')->group(function () {
    
    Route::match(['get', 'post'], '/', 'AdminController@login');
    
    Route::group(['middleware'=>['admin']],function () {

        Route::get('dashboard', 'AdminController@index')->name('dashboard');
        Route::get('settings', 'AdminController@settings')->name('settings');
        Route::post('check_current_password', 'AdminController@checkCurrentPassword')->name('checkCurrentPassword');
        Route::post('update-current-password', 'AdminController@updateCurrentPassword')->name('updateCurrentPassword');
        Route::match(['get','post'],'update-admin-details', 'AdminController@updateAdminDetails')->name('updateAdminDetails');

        // Sections Routes
        Route::resource('sections','SectionController');

        // Brands Routes
        Route::resource('brands','BrandsController');
        Route::get('deleteBrand/{id}', 'BrandsController@deleteBrand');
        Route::patch('updateBrandStatus/{id}', 'BrandsController@updateBrandStatus');

        // Categories Routes
        Route::resource('categories', 'CategoriesController');
        Route::patch('categoryStatusUpdate/{id}', 'CategoriesController@categoryStatusUpdate');
        Route::get('getCategoryBySectionId/{id}', 'CategoriesController@getCategoryBySectionId');
        Route::get('getCategoryBySectionIdForProduct/{id}', 'CategoriesController@getCategoryBySectionIdForProduct');
        Route::get('deleteCategory/{id}', 'CategoriesController@deleteCategory');
        Route::get('deleteCategoryImage/{id}', 'CategoriesController@deleteCategoryImage');
        
        // Product Routes
        Route::resource('products', 'ProductsController');
        Route::patch('productStatusUpdate/{id}', 'ProductsController@productStatusUpdate');
        Route::get('getCategoriesBySectionId/{id}', 'ProductsController@getCategoriesBySectionId');
        Route::get('deleteProductMainImage/{id}', 'ProductsController@deleteProductMainImage');
        Route::get('deleteProductVideo/{id}', 'ProductsController@deleteProductVideo');
        Route::get('deleteProduct/{id}', 'ProductsController@deleteProduct');
        
        // Attributes Routes
        Route::resource('attributes', 'ProductAttributesController');
        Route::patch('updateAttributeStatus/{id}', 'ProductAttributesController@updateAttributeStatus');
        Route::get('deleteAttribute/{id}', 'ProductAttributesController@deleteAttribute');

        // Product Images Routes
        Route::resource('productImages', 'ProductImagesController');
        Route::patch('updateProductImageStatus/{id}', 'ProductImagesController@updateProductImageStatus');
        Route::get('deleteProductImage/{id}', 'ProductImagesController@deleteProductImage');

        Route::get('logout', 'AdminController@logout')->name('logout');
        
    });
});