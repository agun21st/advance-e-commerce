// Jquery Ready Function
$(function(){
    
     // Datatable init
     $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    // $('#example2').DataTable({
    //     "paging": true,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": true,
    //     "info": true,
    //     "autoWidth": false,
    //     "responsive": true,
    // });

    $("#current_password").on("focusout", function(){
        var cPassword = $("#current_password").val();
        $.ajax({
            type: 'post',
            url: '/admin/check_current_password',
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            data: {cPassword: cPassword},
            success: function(res){
                if(res=="false"){
                $("#verifyCPassword").html("<font color=red>Current Password is incorrect</font>");
                }else if(res=="true"){
                $("#verifyCPassword").html("<font color=Blue>Current Password is correct</font>");
                }else{
                $("#verifyCPassword").html("<font color=red>Password is typing...</font>");
                }
            },
            error: function(){
                console.log("Error");
            }
        });

    });
    //Initialize Select2 Elements
    $('.select2').select2();
    //Initialize Select2 Elements
    $('.select2bs4').select2({
    theme: 'bootstrap4'
    });
    // Custom File updload
    bsCustomFileInput.init();
    // Edit Categories Level
    $('#edit_section_id').on("change", function(){
        $.ajax({
            type: 'get',
            url:'/admin/getCategoryBySectionId/'+$(this).val(),
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                console.log(res.categories_dropdown);
                $('#parent_id').html(res.categories_dropdown);
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });
    });
    // Append Categories Level
    $('#section_id').on("change", function(){

        $.ajax({
            type: 'get',
            // url:'/admin/categories/'+$(this).val(),
            url:'/admin/getCategoryBySectionId/'+$(this).val(),
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                console.log(res);
                // $('#appendCategoriesLevel').html(res);
                $('#parent_id').html(res.categories_dropdown);
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });
    });
    // Append Categories Level
    $('#section_id_for_product').on("change", function(){

        $.ajax({
            type: 'get',
            // url:'/admin/categories/'+$(this).val(),
            url:'/admin/getCategoryBySectionIdForProduct/'+$(this).val(),
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                console.log(res);
                // $('#appendCategoriesLevel').html(res);
                $('#category_id_for_product').html(res.categories_dropdown);
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });
    });
    // Active Insactive  Section Status
    $(".updateSectionStatus").on("click", function (){
        var status = $(this).children('i').attr("status");
        var section_id = $(this).attr("section_id");
        
        $.ajax({
            type: 'patch',
            url: '/admin/sections/'+section_id,
            data: {status:status,section_id:section_id},
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                if(res['status']==0)
                {
                  $("#section-"+section_id).html("<i class='fas fa-toggle-off' status='Inactive' style='color:red' title='Active'></i>");
                }else if(res['status']==1){
                  $("#section-"+section_id).html("<i class='fas fa-toggle-on' status='Active' title='Inactive'></i>");
                }
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });

    });
    // Active Inactive Category Status
    $(".updateCategoryStatus").on("click", function (){
        var status = $(this).children('i').attr("status");
        var category_id = $(this).attr("category_id");
        
        $.ajax({
            type: 'patch',
            url: '/admin/categoryStatusUpdate/'+category_id,
            data: {status:status,category_id:category_id},
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                if(res['status']==0)
                {
                  $("#category-"+category_id).html("<i class='fas fa-toggle-off' status='Inactive' style='color:red' title='Active'></i>");
                }else if(res['status']==1){
                  $("#category-"+category_id).html("<i class='fas fa-toggle-on' status='Active' title='Inactive'></i>");
                }
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });

    });

    // Active Inactive Product Status
    $(".updateProductStatus").on("click", function (){
        var status = $(this).children('i').attr("status");
        var product_id = $(this).attr("product_id");
        
        $.ajax({
            type: 'patch',
            url: '/admin/productStatusUpdate/'+product_id,
            data: {status:status,product_id:product_id},
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                if(res['status']==0)
                {
                  $("#product-"+product_id).html("<i class='fas fa-toggle-off' status='Inactive' style='color:red' title='Active'></i>");
                }else if(res['status']==1){
                  $("#product-"+product_id).html("<i class='fas fa-toggle-on' status='Active' title='Inactive'></i>");
                }
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });

    });

    // Active Inactive Attribute Status
    $(".updateAttributeStatus").on("click", function (){
        var status = $(this).children('i').attr("status");
        var attribute_id = $(this).attr("attribute_id");
        
        $.ajax({
            type: 'patch',
            url: '/admin/updateAttributeStatus/'+attribute_id,
            data: {status:status,attribute_id:attribute_id},
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                if(res['status']==0)
                {
                  $("#attribute-"+attribute_id).html("<i class='fas fa-toggle-off' status='Inactive' style='color:red' title='Active'></i>");
                }else if(res['status']==1){
                  $("#attribute-"+attribute_id).html("<i class='fas fa-toggle-on' status='Active' title='Inactive'></i>");
                }
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });

    });

    // Active Inactive Product Images Status
    $(".updateProductImageStatus").on("click", function (){
        var status = $(this).children('i').attr("status");
        var productImage_id = $(this).attr("productImage_id");
        $.ajax({
            type: 'patch',
            url: '/admin/updateProductImageStatus/'+productImage_id,
            data: {status:status,productImage_id:productImage_id},
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                if(res['status']==0)
                {
                  $("#productImage-"+productImage_id).html("<i class='fas fa-toggle-off' status='Inactive' style='color:red' title='Active'></i>");
                }else if(res['status']==1){
                  $("#productImage-"+productImage_id).html("<i class='fas fa-toggle-on' status='Active' title='Inactive'></i>");
                }
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });

    });

    // Active Inactive Brand Status
    $(".updateBrandStatus").on("click", function (){
        var status = $(this).children('i').attr("status");
        var brand_id = $(this).attr("brand_id");
        $.ajax({
            type: 'patch',
            url: '/admin/updateBrandStatus/'+brand_id,
            data: {status:status,brand_id:brand_id},
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function(res){
                if(res['status']==0)
                {
                  $("#brand-"+brand_id).html("<i class='fas fa-toggle-off' status='Inactive' style='color:red' title='Active'></i>");
                }else if(res['status']==1){
                  $("#brand-"+brand_id).html("<i class='fas fa-toggle-on' status='Active' title='Inactive'></i>");
                }
            },
            error:function(){
                console.log("Ajax error occured");
            }
        });

    });



    // Confirm Deletaion of Record by JQuery
    // $(".confirmDelete").on("click", function(){
    //     if(confirm("Are you sure to deleted this?")){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // });

    // Common Confirm Deletaion of Record by JQuery
    $(".confirmDelete").on("click", function(){
        var record      =   $(this).attr("record");
        var recordName  =   $(this).attr("recordName");
        var recordId    =   $(this).attr("recordId");
        Swal.fire({
            title: 'Are you sure deleted this '+recordName+'?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                // Swal.fire(
                //     'Deleted!',
                //     'Your '+recordName+' has been deleted.',
                //     'success'
                // )
                window.location.href  =   "/admin/"+record+"/"+recordId;
            }
          })
    });


    // Products Attributes add/remove Script
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div class="mt-2"><input type="text" name="size[]" id="size" placeholder="SIZE" value="" style="width: 120px;" required/> <input type="number" name="price[]" id="price" placeholder="PRICE" value="" style="width: 120px;" required/> <input type="number" name="stock[]" id="stock" placeholder="STOCK" value="" style="width: 120px;" required/> <input type="text" name="sku[]" id="sku" placeholder="SKU" value="" style="width: 120px;" required/><a href="javascript:void(0);" class="remove_button" title="Remove field">&nbsp;&nbsp;Remove</a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).on('click', function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
    // Products Attributes add/remove Script End


    // remove alert bar
    window.setTimeout(function() {
        $(".alert").fadeTo(300, 0).slideUp(300, function(){
            $(this).remove(); 
        });
    }, 4000);
});